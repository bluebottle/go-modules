package misc

import (
	"reflect"
	"testing"
)

// TestAddToSlice tests the AddToSlice function.
func TestAddToSlice(t *testing.T) {
	tables := []struct {
		origin []string
		insert string
		result []string
	}{
		{[]string{"1", "2"}, "5", []string{"1", "2", "5"}}, // Add a not existing value
		{[]string{"1", "2"}, "2", []string{"1", "2"}},      // Add an existing value
	}

	for _, table := range tables {
		result := AddToSlice(table.origin, table.insert)

		if len(result) != len(table.result) || !reflect.DeepEqual(result, table.result) {
			t.Errorf("AddToSlice was incorrect, got: %s, want: %s.", result, table.result)
		} // if
	} // for
} // TestAddToSlice ()

// TestRemoveFromSlice tests the RemoveFromSlice function.
func TestRemoveFromSlice(t *testing.T) {
	tables := []struct {
		origin []string
		remove string
		result []string
	}{
		{[]string{"1", "2", "3"}, "5", []string{"1", "2", "3"}}, // Remove a not existing value
		{[]string{"1", "2", "3"}, "2", []string{"1", "3"}},      // Remove an existing value
	}

	for _, table := range tables {
		result := RemoveFromSlice(table.origin, table.remove)

		if len(result) != len(table.result) || !reflect.DeepEqual(result, table.result) {
			t.Errorf("RemoveFromSlice was incorrect, got: %s, want: %s.", result, table.result)
		} // if
	} // for
} // TestRemoveFromSlice ()
