package misc

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"testing"
)

func TestErrorPrint(t *testing.T) {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	ShowError(errors.New("Test error"), "", "ErrPrint")
	t.Log(buf.String())
	log.SetOutput(os.Stderr)
} // TestErrorPrint ()

func TestErrorPanic(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			err, ok := r.(error)

			if err == nil && !ok {
				err = fmt.Errorf("pkg: %v", r)
				fmt.Println(err)
			} // if
		} // if
	}()
	ShowError(errors.New("Test error"), "", "ErrPanic")
	t.Errorf("should have panicked")
} // TestErrorPanic ()
