// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/bluebottle/go-modules/postgresql"
)

// Testing FastStochastic function.
func TestFastStochastic(t *testing.T) {
	quoteVal := []postgresql.Quote{
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.0090, Low: 125.3574, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.6159, Low: 126.1633, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.5911, Low: 124.9296, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.3472, Low: 126.0937, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.1730, Low: 126.8199, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.4317, Low: 126.4817, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.3671, Low: 126.0340, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.4220, Low: 124.8301, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.8995, Low: 126.3921, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.8498, Low: 125.7156, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 125.6460, Low: 124.5615, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 125.7156, Low: 124.5715, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.1582, Low: 125.0689, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.7154, Low: 126.8597, Close: 127.2876, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.6855, Low: 126.6309, Close: 127.1781, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2228, Low: 126.8001, Close: 128.0138, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2725, Low: 126.7105, Close: 127.1085, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.0934, Low: 126.8001, Close: 127.7253, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2725, Low: 126.1335, Close: 127.0587, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.7353, Low: 125.9245, Close: 127.3273, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.7700, Low: 126.9891, Close: 128.7103, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.2873, Low: 127.8148, Close: 127.8745, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 130.0633, Low: 128.4715, Close: 128.5809, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.1182, Low: 128.0641, Close: 128.6008, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.2873, Low: 127.6059, Close: 127.9342, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.4715, Low: 127.5960, Close: 128.1133, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.0934, Low: 126.9990, Close: 127.5960, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.6506, Low: 126.8995, Close: 127.5960, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.1381, Low: 127.4865, Close: 128.6904, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.6406, Low: 127.3970, Close: 128.2725, Volume: 0},
	}
	result := FullStoch{
		[]float64{70.43822024701552, 67.60890910030484, 89.20210841816956, 65.81055242623131, 81.74771329647047, 64.52379721978191, 74.5297763406087, 98.58144231911558, 70.10453256591492, 73.05609073394193, 73.4177905412773, 61.23129028733758, 60.95627102354659, 40.38610225186062, 40.38610225186062, 66.8285493379727, 56.73141973518921},
		[]float64{75.74974592182998, 74.20718998156856, 78.92012471362378, 70.69402098082789, 73.60042895228702, 79.2116719598354, 81.07191707521307, 80.58068853965749, 72.19280461371139, 69.23505718751893, 65.20178395072048, 54.1912211875816, 47.24282517575595, 49.20025128056465, 54.64869044167418},
		14, 0, 3,
	}

	total := FastStochastic(quoteVal, 14)

	if !reflect.DeepEqual(total, result) {
		t.Errorf("FastStochastic was incorrect, got: %v, want: %v.", total, result)
	} // if
} // TestFastStochastic ()

// Testing SlowStochastic function.
func TestSlowStochastic(t *testing.T) {
	quoteVal := []postgresql.Quote{
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.0090, Low: 125.3574, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.6159, Low: 126.1633, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.5911, Low: 124.9296, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.3472, Low: 126.0937, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.1730, Low: 126.8199, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.4317, Low: 126.4817, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.3671, Low: 126.0340, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.4220, Low: 124.8301, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.8995, Low: 126.3921, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.8498, Low: 125.7156, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 125.6460, Low: 124.5615, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 125.7156, Low: 124.5715, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.1582, Low: 125.0689, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.7154, Low: 126.8597, Close: 127.2876, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.6855, Low: 126.6309, Close: 127.1781, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2228, Low: 126.8001, Close: 128.0138, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2725, Low: 126.7105, Close: 127.1085, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.0934, Low: 126.8001, Close: 127.7253, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2725, Low: 126.1335, Close: 127.0587, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.7353, Low: 125.9245, Close: 127.3273, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.7700, Low: 126.9891, Close: 128.7103, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.2873, Low: 127.8148, Close: 127.8745, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 130.0633, Low: 128.4715, Close: 128.5809, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.1182, Low: 128.0641, Close: 128.6008, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.2873, Low: 127.6059, Close: 127.9342, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.4715, Low: 127.5960, Close: 128.1133, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.0934, Low: 126.9990, Close: 127.5960, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.6506, Low: 126.8995, Close: 127.5960, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.1381, Low: 127.4865, Close: 128.6904, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.6406, Low: 127.3970, Close: 128.2725, Volume: 0},
	}
	result := FullStoch{
		[]float64{75.74974592182998, 74.20718998156856, 78.92012471362378, 70.69402098082789, 73.60042895228702, 79.2116719598354, 81.07191707521307, 80.58068853965749, 72.19280461371139, 69.23505718751893, 65.20178395072048, 54.1912211875816, 47.24282517575595, 49.20025128056465, 54.64869044167418},
		[]float64{76.29235353900744, 74.60711189200674, 74.40485821557957, 74.50204063098343, 77.96133932911182, 80.28809252490198, 77.94847007619397, 74.00285011362926, 68.8765485839836, 62.876020775273666, 55.54527677135267, 50.2114325479674, 50.36392229933159},
		14, 3, 3,
	}

	total := SlowStochastic(quoteVal, 14)

	if !reflect.DeepEqual(total, result) {
		t.Errorf("SlowStochastic was incorrect, got: %v, want: %v.", total, result)
	} // if
} // TestSlowStochastic ()

func TestEvaluateValues(t *testing.T) {
	type testValues struct {
		values []float64
		level  float64
		result Evaluate
	}

	testVal := []testValues{
		{[]float64{10.0, 20.0, 30.0, 40.0, 50.0}, 20.0, Evaluate{IsLower: false, CurrentValue: 50.0}},
		{[]float64{10.0, 20.0, 30.0, 40.0, 50.0}, 60.0, Evaluate{IsLower: true, CurrentValue: 50.0}},
	}

	for _, test := range testVal {
		results := EvaluateValues(test.values, test.level)

		if !reflect.DeepEqual(results, test.result) {
			t.Errorf("evaluateValues was incorrect, got: %v, want: %v.", results, test.result)
		} // if
	} // for
} // TestevaluateValues ()
