// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"reflect"
	"testing"

	"gitlab.com/bluebottle/go-modules/postgresql"
)

// Testing SMA function.
func TestSMA(t *testing.T) {
	tables := []struct {
		period int
		values []float64
		result []float64
	}{
		{5, []float64{11, 12, 13, 14, 15, 16, 17}, []float64{13, 14, 15}},
		{5, []float64{11, 12, 13, 14, 15, 16, 17, 18, 19, 20}, []float64{13, 14, 15, 16, 17, 18}},
		{10, []float64{11, 12, 13, 14, 15, 16, 17, 18, 19, 20}, []float64{15.5}},
	}

	for _, table := range tables {
		total := SMA(table.period, table.values)

		if !reflect.DeepEqual(total, table.result) {
			t.Errorf("SMA was incorrect, got: %v, want: %v.", total, table.result)
		} // if
	} // for
} // TestSMA ()

func TestEmaDiffPer(t *testing.T) {
	type testValues struct {
		quotes []postgresql.Quote
		ema    []float64
		result float64
	}

	testVal := []testValues{
		{[]postgresql.Quote{
			{Close: 10.0},
			{Close: 20.0},
			{Close: 30.0},
		},
			[]float64{10.0, 20.0, 30.0, 33.0}, -9.090909090909092},
		{[]postgresql.Quote{
			{Close: 10.0},
			{Close: 20.0},
			{Close: 33.0},
		},
			[]float64{10.0, 20.0, 30.0, 30.0}, 10},
		{[]postgresql.Quote{
			{Close: 20.0},
			{Close: 40.0},
			{Close: 60.0},
		},
			[]float64{20.0, 40.0, 60.0, 54.0}, 11.11111111111111},
		{[]postgresql.Quote{
			{Close: 20.0},
			{Close: 40.0},
			{Close: 54.0},
		},
			[]float64{20.0, 40.0, 60.0, 60.0}, -10},
		{[]postgresql.Quote{},
			[]float64{20.0, 40.0, 60.0, 60.0}, 0},
	}

	for _, test := range testVal {
		results := EmaDiffPer(test.ema, test.quotes)

		if !reflect.DeepEqual(results, test.result) {
			t.Errorf("emaDiffPer was incorrect, got: %v, want: %v.", results, test.result)
		} // if
	} // for
} // TestEmaDiffPer ()
