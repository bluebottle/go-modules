// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/bluebottle/go-modules/postgresql"
)

func TestWilliamsRP(t *testing.T) {
	quoteVal := []postgresql.Quote{
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.0090, Low: 125.3574, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.6159, Low: 126.1633, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.5911, Low: 124.9296, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.3472, Low: 126.0937, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.1730, Low: 126.8199, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.4317, Low: 126.4817, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.3671, Low: 126.0340, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.4220, Low: 124.8301, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.8995, Low: 126.3921, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 126.8498, Low: 125.7156, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 125.6460, Low: 124.5615, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 125.7156, Low: 124.5715, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.1582, Low: 125.0689, Close: 0.0, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.7154, Low: 126.8597, Close: 127.2876, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.6855, Low: 126.6309, Close: 127.1781, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2228, Low: 126.8001, Close: 128.0138, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2725, Low: 126.7105, Close: 127.1085, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.0934, Low: 126.8001, Close: 127.7253, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.2725, Low: 126.1335, Close: 127.0587, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 127.7353, Low: 125.9245, Close: 127.3273, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.7700, Low: 126.9891, Close: 128.7103, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.2873, Low: 127.8148, Close: 127.8745, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 130.0633, Low: 128.4715, Close: 128.5809, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.1182, Low: 128.0641, Close: 128.6008, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.2873, Low: 127.6059, Close: 127.9342, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.4715, Low: 127.5960, Close: 128.1133, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.0934, Low: 126.9990, Close: 127.5960, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.6506, Low: 126.8995, Close: 127.5960, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 129.1381, Low: 127.4865, Close: 128.6904, Volume: 0},
		{Symbol: "Test", QuoteDate: time.Now(), Open: 0.0, High: 128.6406, Low: 127.3970, Close: 128.2725, Volume: 0},
	}
	result := WilliamsR{
		[]float64{-29.561779752984485, -32.391090899695165, -10.797891581830443, -34.189447573768696, -18.252286703529535, -35.476202780218095, -25.470223659391287, -1.4185576808844131, -29.89546743408507, -26.943909266058082, -26.582209458722687, -38.76870971266242, -39.04372897645341, -59.61389774813938, -59.61389774813938, -33.171450662027304, -43.26858026481079},
	}

	total := WilliamsRP(quoteVal, 14)

	if !reflect.DeepEqual(total, result) {
		t.Errorf("FastStochastic was incorrect, got: %v, want: %v.", total, result)
	} // if
} // TestWilliamsRP ()
