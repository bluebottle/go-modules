// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"testing"
)

// Testing CalculateCurrentRisk function.
func TestCalculateCurrentRisk(t *testing.T) {
	//
	// TODO: redo test when CalculateCurrentRisk is redone
	//
	// tables := []struct {
	// 	trade  postgresql.Trade
	// 	result float64
	// }{
	// 	{postgresql.Trade{BuyPrice: 10.0, BuyQuantity: 100.0, CurrPrice: 20.0, StopLossValue: 15.0}, 500.0},
	// 	{postgresql.Trade{BuyPrice: 10.0, BuyQuantity: 100.0, CurrPrice: 11.0, StopLossValue: 8.0}, 300.0},
	// 	{postgresql.Trade{BuyPrice: 10.0, BuyQuantity: 100.0, CurrPrice: 9.0, StopLossValue: 8.0}, 100.0},
	// 	{postgresql.Trade{BuyPrice: 10.0, BuyQuantity: 100.0, CurrPrice: 9.0, StopLossValue: 0.0}, 1000.0},
	// 	{postgresql.Trade{BuyPrice: 10.0, BuyQuantity: 100.0, CurrPrice: 8.0, StopLossValue: 9.0}, 800.0},
	// }

	// for _, table := range tables {
	// 	total := CalculateCurrentRisk(&table.trade)

	// 	if !reflect.DeepEqual(total, table.result) {
	// 		t.Errorf("CalculateCurrentRisk was incorrect, got: %v, want: %v.", total, table.result)
	// 	} // if
	// } // for
} // TestCalculateCurrentRisk ()
