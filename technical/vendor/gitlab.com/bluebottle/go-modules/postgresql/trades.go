// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Trade contains the structure of a trades sentence
type Trade struct {
	TradeID         uint64
	PortfolioID     uint64
	Symbol          string
	Comment         string
	CheckLevelValue float64
	StopLossType    string
	StopLossValue   float64
}

// NewTrade returns an empty trade
func NewTrade() Trade {
	return Trade{
		TradeID:         0,
		PortfolioID:     0,
		Symbol:          "",
		Comment:         "",
		CheckLevelValue: 0.0,
		StopLossType:    "",
		StopLossValue:   0.0,
	}
} // NewTrade ()

// FindAllTrades finds all trade values.
func FindAllTrades() (tmpTrade []Trade, err error) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	tmpRows := FindAllTradeRows("")

	for tmpRows.Next() {
		tmpTradeRow, err2 := GetNextTradeRow(tmpRows)

		if err2 != nil {
			err = err2
		} // if

		tmpTrade = append(tmpTrade, tmpTradeRow)
	} // for

	conn.Release()
	return
} // FindAllTrades()

// FindPortfolioTrades finds all trades for given portfolio_id.
func FindPortfolioTrades(portID uint64) (tmpTrade []Trade, err error) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	tmpRows := FindAllTradeRows(fmt.Sprintf(" where portfolio_id=%d", portID))

	for tmpRows.Next() {
		tmpTradeRow, err2 := GetNextTradeRow(tmpRows)

		if err2 != nil {
			err = err2
		} // if

		tmpTrade = append(tmpTrade, tmpTradeRow)
	} // for

	conn.Release()
	return
} // FindPortfolioTrades()

// FindAllTradeRows finds all trade values.
func FindAllTradeRows(whereClause string) pgx.Rows {
	rows, err = conn.Query(context.Background(), "select trade_id, portfolio_id, symbol, comment, check_level_value, stop_loss_type, stop_loss_value from public.trades "+whereClause)
	misc.ShowError(err, "", "ErrPanic")
	return rows
} // FindAllTradeRows()

// FindTrade finds the row in the trades table for a given portfolio id.
func FindTrade(portID int64) pgx.Rows {
	rows, err = conn.Query(context.Background(), fmt.Sprintf("select trade_id, portfolio_id, symbol, comment, check_level_value, stop_loss_type, stop_loss_value from public.trades where portfolio_id=%d", portID))
	misc.ShowError(err, "", "ErrPanic")
	return rows
} // FindTrade()

// GetNextTradeRow returns the next trade row.
func GetNextTradeRow(rows pgx.Rows) (trade Trade, err error) {
	err = rows.Scan(&trade.TradeID, &trade.PortfolioID, &trade.Symbol, &trade.Comment, &trade.CheckLevelValue, &trade.StopLossType, &trade.StopLossValue)
	return
} // GetNextTradeRow ()
