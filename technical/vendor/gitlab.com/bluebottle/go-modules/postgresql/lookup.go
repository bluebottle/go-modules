// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Lookup contains the structure of a lookup sentence
type Lookup struct {
	Symbol        string
	Name          string
	WKN           string
	ISIN          string
	LookType      string
	QuoteCurrency string
	IsHistory     bool
}

// NewLookup returns an empty lookup
func NewLookup() Lookup {
	return Lookup{
		Symbol:        "",
		Name:          "",
		WKN:           "",
		ISIN:          "",
		LookType:      "",
		QuoteCurrency: "",
		IsHistory:     false,
	}
} // NewLookup ()

var lookup Lookup

// translateFields translates input string to database field name
func translateFields(input string) (output string) {
	switch input {
	case "Type":
		output = "look_type"
	case "Currency":
		output = "quote_currency"
	case "Historic":
		output = "is_history"
	default:
		output = strings.ToLower(input)
	} // switch

	return
} // translateFields()

// translateDir translates input string to sort direction
func translateDir(input string) (output string) {
	switch input {
	case "up":
		output = "asc"
	case "down":
		output = "desc"
	default:
		output = strings.ToLower(input)
	} // switch

	return
} // translateDir()

// FindAllLookup finds all lookup values.
func FindAllLookup() []Lookup {
	return FindAllLookupOrdered("symbol", "asc")
} // FindAllLookup()

// defaultFind generic lookup function
func defaultFind(clause string) []Lookup {
	var tmpLookup []Lookup
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	tmpRows := FindAllLookupRows(clause)

	for tmpRows.Next() {
		tmpLookup = append(tmpLookup, GetNextLookupRow(tmpRows))
	} // for

	conn.Release()
	return tmpLookup
} // defaultFind()

// FindAllLookupOrdered finds all lookup values ordered by column and direction.
func FindAllLookupOrdered(column, direction string) []Lookup {
	return defaultFind("order by " + translateFields(column) + " " + translateDir(direction))
} // FindAllLookupOrdered()

// FindAllCurrentLookup finds all lookup values which are not test indices and not historic.
func FindAllCurrentLookup() []Lookup {
	return FindAllCurrentLookupOrdered("symbol", "asc")
} // FindAllCurrentLookup()

// FindAllCurrentLookupOrdered finds all lookup values which are not test indices and not historic ordered by column and direction.
func FindAllCurrentLookupOrdered(column, direction string) []Lookup {
	return defaultFind("where look_type != 'Testindex' and is_history = false order by " + translateFields(column) + " " + translateDir(direction))
} // FindAllCurrentLookupOrdered()

// FindAllHistoricLookup finds all lookup values which are not test indices and not historic.
func FindAllHistoricLookup() []Lookup {
	return FindAllHistoricLookupOrdered("symbol", "asc")
} // FindAllHistoricLookup()

// FindAllHistoricLookupOrdered finds all lookup values which are not test indices and is historic ordered by column and direction.
func FindAllHistoricLookupOrdered(column, direction string) []Lookup {
	return defaultFind("where look_type != 'Testindex' and is_history = true order by " + translateFields(column) + " " + translateDir(direction))
} // FindAllHistoricLookupOrdered()

// FindAllLookupRows finds all lookup values.
func FindAllLookupRows(whereClause string) pgx.Rows {
	rows, err = conn.Query(context.Background(), "select * from public.lookup "+whereClause)
	misc.ShowError(err, "", "ErrPanic")
	return rows
} // FindAllLookupRows()

// FindLookup finds the row in the lookup table for a given symbol.
func FindLookup(symbol string) pgx.Rows {
	rows, err = conn.Query(context.Background(), "select symbol, name, wkn, isin, look_type, quote_currency, is_history from public.lookup where symbol='"+symbol+"'")
	misc.ShowError(err, "", "ErrPanic")
	return rows
} // FindLookup()

// GetSingleLookup gets one row in the lookup table for a given symbol.
func GetSingleLookup(symbol string) Lookup {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err := conn.Query(context.Background(), "select symbol, name, wkn, isin, look_type, quote_currency, is_history from public.lookup where symbol='"+symbol+"' limit 1")
	misc.ShowError(err, "", "ErrPanic")

	if rows.Next() {
		lookup = GetNextLookupRow(rows)
	} // if

	conn.Release()
	return lookup
} // GetSingleLookup()

// GetSingleIsinLookup gets one row in the lookup table for a given ISIN.
// If ISIN is not found an empty lookup value is returned
func GetSingleIsinLookup(isin string) Lookup {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err = conn.Query(context.Background(), "select symbol, name, wkn, isin, look_type, quote_currency, is_history from public.lookup where isin='"+isin+"' limit 1")
	misc.ShowError(err, "", "ErrPanic")

	if rows.Next() {
		lookup = GetNextLookupRow(rows)
	} else {
		lookup = NewLookup()
	} // if

	conn.Release()
	return lookup
} // GetSingleIsinLookup()

// GetNextLookupRow returns the next lookup row.
func GetNextLookupRow(rows pgx.Rows) Lookup {
	err := rows.Scan(&lookup.Symbol, &lookup.Name, &lookup.WKN, &lookup.ISIN, &lookup.LookType, &lookup.QuoteCurrency, &lookup.IsHistory)
	misc.ShowError(err, "", "ErrPanic")
	return lookup
} // GetNextLookupRow ()

// AddSymbol will add a new symbol. If it already exists, nothing is done.
func AddSymbol(symbol, name, wkn, isin, sector, lookType, quoteCurrency string) {
	conn, err := pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	err = insertSymbol(nil, conn, symbol, name, wkn, isin, lookType, quoteCurrency)
	misc.ShowError(err, "", "ErrPanic")
	conn.Release()
} // AddSymbol ()

// insertSymbol does the real insertion into the database
func insertSymbol(tx pgx.Tx, conn *pgxpool.Conn, symbol, name, wkn, isin, lookType, quoteCurrency string) (err error) {
	cmd := fmt.Sprintf("insert into public.lookup(symbol, name, wkn, isin, lookType, quoteCurrency) values ('%s', '%s', '%s', '%s', '%s', '%s') on conflict do nothing", symbol, name, wkn, isin, lookType, quoteCurrency)

	if tx == nil {
		_, err = conn.Exec(context.Background(), cmd)
	} else {
		_, err = tx.Exec(context.Background(), cmd)
	} // if

	return
} // insertSymbol ()

// AddSymbolWithWarrant will add a new symbol for a warrant. If it already exists, nothing is done. The warrant information is also added to its table.
func AddSymbolWithWarrant(symbol, name, wkn, isin, lookType, quoteCurrency, basesym, optionend string) {
	fmt.Println("WithWarrant")
	conn, err := pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	tx, err := conn.BeginTx(context.Background(), pgx.TxOptions{})
	misc.ShowError(err, "", "ErrPanic")
	err = insertSymbol(tx, conn, symbol, name, wkn, isin, lookType, quoteCurrency)

	if err == nil {
		err = insertWarrant(tx, conn, symbol, basesym, optionend)
	} // if

	if err != nil {
		err = tx.Rollback(context.Background())
	} else {
		err = tx.Commit(context.Background())
	} // if

	misc.ShowError(err, "", "ErrPanic")
	conn.Release()
} // AddSymbolWithWarrant ()
