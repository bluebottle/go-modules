// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v5"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Transaction contains the structure of a transaction row
type Transaction struct {
	TransactionID uint64
	TradeID       uint64
	PortfolioID   uint64
	Symbol        string
	TransDate     time.Time
	Action        string
	Price         float64
	Quantity      float64
	Brokerage     float64
	Slippage      float64
}

// NewTransaction returns an empty transaction
func NewTransaction() Transaction {
	return Transaction{
		TransactionID: 0,
		TradeID:       0,
		PortfolioID:   0,
		Symbol:        "",
		TransDate:     time.Time{},
		Action:        "",
		Price:         0.0,
		Quantity:      0.0,
		Brokerage:     0.0,
		Slippage:      0.0,
	}
} // NewTransaction ()

// // FindAllTransactions finds all transaction values.
// func FindAllTransactions() (tmpTrans []Transaction, err error) {
// 	conn, err = pool.Acquire(context.Background())
// 	misc.ShowError(err, "", "ErrPanic")
// 	tmpRows := FindAllTransRows("")

// 	for tmpRows.Next() {
// 		tmpTransRow, err2 := GetNextTransRow(tmpRows)

// 		if err2 != nil {
// 			err = err2
// 		} // if

// 		tmpTrans = append(tmpTrans, tmpTransRow)
// 	} // for

// 	conn.Release()
// 	return
// } // FindAllTransactions()

// FindPortTradeTransactions finds all Transactions for given portfolio_id and trade_id
func FindPortTradeTransactions(portID, tradeID uint64) (tmpTrans []Transaction, err error) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	tmpRows := FindAllTransRows(fmt.Sprintf(" where portfolio_id=%d and trade_id=%d", portID, tradeID))

	for tmpRows.Next() {
		tmpTransRow, err2 := GetNextTransRow(tmpRows)

		if err2 != nil {
			err = err2
		} // if

		tmpTrans = append(tmpTrans, tmpTransRow)
	} // for

	conn.Release()
	return
} // FindPortTradeTransactions()

// // FindPortfolioTransactions finds all Transactions for given portfolio_id.
// func FindPortfolioTransactions(portID uint64) (tmpTrans []Transaction, err error) {
// 	conn, err = pool.Acquire(context.Background())
// 	misc.ShowError(err, "", "ErrPanic")
// 	tmpRows := FindAllTransRows(fmt.Sprintf(" where portfolio_id=%d", portID))

// 	for tmpRows.Next() {
// 		tmpTransRow, err2 := GetNextTransRow(tmpRows)

// 		if err2 != nil {
// 			err = err2
// 		} // if

// 		tmpTrans = append(tmpTrans, tmpTransRow)
// 	} // for

// 	conn.Release()
// 	return
// } // FindPortfolioTransactions()

// FindAllTransRows finds all transaction values.
func FindAllTransRows(whereClause string) pgx.Rows {
	rows, err = conn.Query(context.Background(), "select * from public.Transactions "+whereClause)
	misc.ShowError(err, "", "ErrPanic")
	return rows
} // FindAllTransRows()

// // FindTrans finds the row in the Transactions table for a given portfolio id.
// func FindTrans(portID int64) pgx.Rows {
// 	rows, err = conn.Query(context.Background(), fmt.Sprintf("select * from public.Transactions where portfolio_id=%d", portID))
// 	misc.ShowError(err, "", "ErrPanic")
// 	return rows
// } // FindTrans()

// GetNextTransRow returns the next transaction row.
func GetNextTransRow(rows pgx.Rows) (trans Transaction, err error) {
	var (
		tmpTransdate pgtype.Timestamptz
	)

	err = rows.Scan(&trans.TransactionID, &trans.TradeID, &trans.PortfolioID, &trans.Symbol, &tmpTransdate, &trans.Action, &trans.Price, &trans.Quantity, &trans.Brokerage, &trans.Slippage)
	misc.ShowError(err, "", "ErrPanic")
	// move converted dates to lookup
	trans.TransDate = tmpTransdate.Time
	return
} // GetNextTransRow ()
