// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// Calculating exponential moving average
// EMA = K * (Current price - Previous EMA) + Previous EMA
// K = smoothing / (period + 1)
func ema(period int, values []float64, smoothing float64) []float64 {
	// tmp := (1 + math.Sqrt(5)) / 2
	// fmt.Println(tmp)
	var emaVal []float64
	var weigthMult = smoothing / float64(period+1)

	for i := 0; i < len(values); i++ {
		if i == 0 {
			emaVal = append(emaVal, values[i])
		} else {
			emaVal = append(emaVal, (weigthMult*values[i])+(emaVal[i-1]*(1-weigthMult)))
		} // if
	} // for

	return emaVal
} // ema ()

// EMA Exponential moving average
func EMA(period int, values []float64) []float64 {
	return ema(period, values, 2)
} // EMA ()

// SMA Simple Moving Average
// Daily Closing Prices: 11,12,13,14,15,16,17
// First day of 5-day SMA: (11 + 12 + 13 + 14 + 15) / 5 = 13
// Second day of 5-day SMA: (12 + 13 + 14 + 15 + 16) / 5 = 14
// Third day of 5-day SMA: (13 + 14 + 15 + 16 + 17) / 5 = 15
func SMA(period int, values []float64) (outValues []float64) {
	for i := period; i <= len(values); i++ {
		outValues = append(outValues, calcAvg(values[i-period:i], period))
	} // for

	return
} // SMA ()

func calcAvg(values []float64, period int) (avg float64) {
	tmpSum := 0.0

	for _, v := range values {
		tmpSum += v
	} // for

	return tmpSum / float64(period)
} // calcAvg ()

// EmaDiffPer returns the difference from current close to ema in percent (positive or negative)
func EmaDiffPer(ema []float64, quotes []postgresql.Quote) (diff float64) {
	if len(quotes) > 0 {
		currClose := quotes[len(quotes)-1].Close
		diff = (currClose - ema[len(ema)-1]) * 100 / ema[len(ema)-1]
	} // if

	return
} // EmaDiffPer ()
