// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

// Evaluate shows the current state of a stochastic
type Evaluate struct {
	IsLower      bool
	CurrentValue float64
}

// EvaluateValues check if last given value is lower than desired level. Then return true and the value, else return 0 and false.
func EvaluateValues(values []float64, level float64) (result Evaluate) {
	result.IsLower = false
	result.CurrentValue = -1.0

	if len(values) > 0 {
		result.CurrentValue = values[len(values)-1]

		if result.CurrentValue < level {
			result.IsLower = true
		} // if
	} // if

	return
} // EvaluateValues()
