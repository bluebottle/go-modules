package postgresql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Risk contains the structure of a risk row
type Risk struct {
	Country           string
	MarketRiskPremium float64
	RiskFreeRate      float64
}

// NewRisk returns an empty risk row
func NewRisk() Risk {
	return Risk{
		Country:           "",
		MarketRiskPremium: 0.0,
		RiskFreeRate:      0.0,
	}
} // NewRisk ()

// GetAllRisk gets all existing risk values
func GetAllRisks() (riskArr []Risk) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err = conn.Query(context.Background(), "select country, market_risk_premium, risk_free_rate from public.risk order by country")
	misc.ShowError(err, "", "ErrPanic")

	for rows.Next() {
		riskArr = append(riskArr, GetNextRiskRow(rows))
	} // for

	conn.Release()
	return
} // GetAllRisks()

// GetNextRiskRow returns the next risk row.
func GetNextRiskRow(rows pgx.Rows) (risk Risk) {
	err := rows.Scan(&risk.Country, &risk.MarketRiskPremium, &risk.RiskFreeRate)
	misc.ShowError(err, "", "ErrPanic")
	return
} // GetNextRiskRow ()

// GetAllCountries returns all known countries with risk values.
func GetAllCountries() (countries []string) {
	tmpRisks := GetAllRisks()

	for _, v := range tmpRisks {
		countries = append(countries, v.Country)
	} // for

	return
} // GetAllCountries ()

// GetRiskValuesForCountry returns risk values for given country.
func GetRiskValuesForCountry(country string) (MRP, RFR float64) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err = conn.Query(context.Background(), fmt.Sprintf("select country, market_risk_premium, risk_free_rate from public.risk where country='%s'", country))
	misc.ShowError(err, "", "ErrPanic")

	if rows.Next() {
		tmpRisk := GetNextRiskRow(rows)
		conn.Release()
		return tmpRisk.MarketRiskPremium, tmpRisk.RiskFreeRate
	} else {
		conn.Release()
		return 0.0, 0.0
	} // if
} // GetRiskValuesForCountry ()
