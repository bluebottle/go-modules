// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"crypto/rand"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/bluebottle/go-modules/misc"
)

// UpdateYahoo reads values from query1.finance.yahoo.com and returns a slice of quotes
// symbol should be in the format "yahoo::<symbol>"
func UpdateYahoo(symbol string) (outArr []Quote) {
	var startDate time.Time
	var tmpQV int64
	// get last quote for symbol and extract quote date
	rows := FindQuotes(symbol, "ASC")

	// get last quote date, if we already have quotes
	if len(rows) > 0 {
		startDate = rows[len(rows)-1].QuoteDate
	} else {
		startDate, err = time.Parse("2006-01-02", "2010-01-01")
	} // if

	fmt.Println("Last quote date before update: " + startDate.Format(time.RFC3339))
	// create URL to grep and download data
	// period1 = unix timestamp of begin date
	// preiod2 = unix timestamp of end date
	// https://query1.finance.yahoo.com/v7/finance/download/SIKB.F?period1=1674582427&period2=1706118427&interval=1d&events=history&includeAdjustedClose=true
	beginDate := startDate.AddDate(0, 0, -7)
	endDate := time.Now()
	stream, err := http.Get(fmt.Sprintf("https://query1.finance.yahoo.com/v7/finance/download/%s?period1=%d&period2=%d&interval=1d&events=history&includeAdjustedClose=true", strings.Split(symbol, "::")[1], beginDate.Unix(), endDate.Unix()))
	misc.ShowError(err, "", "ErrPanic")
	content, err := io.ReadAll(stream.Body)
	misc.ShowError(err, "", "ErrPanic")
	err = stream.Body.Close()
	misc.ShowError(err, "", "ErrPanic")
	contentString := string(content)
	//fmt.Println(contentString)

	// split content lines
	for i, v := range strings.Split(contentString, "\n") {
		// skip header lines and line is not empty
		if i > 0 && len(v) > 0 {
			lineSplit := strings.Split(v, ",")
			tmpQD, err := time.Parse("2006-01-02", lineSplit[0])
			misc.ShowError(err, "", "ErrPrint")

			if err == nil && lineSplit[1] != "null" {
				tmpQO, err := strconv.ParseFloat(lineSplit[1], 64)
				misc.ShowError(err, "", "ErrPrint")
				tmpQH, err := strconv.ParseFloat(lineSplit[2], 64)
				misc.ShowError(err, "", "ErrPrint")
				tmpQL, err := strconv.ParseFloat(lineSplit[3], 64)
				misc.ShowError(err, "", "ErrPrint")
				// use adjusted close instead of close
				tmpQC, err := strconv.ParseFloat(lineSplit[5], 64)
				misc.ShowError(err, "", "ErrPrint")

				if len(lineSplit) > 6 {
					tmpQV, err = strconv.ParseInt(lineSplit[6], 10, 64)
					misc.ShowError(err, "", "ErrPrint")
				} else {
					tmpQV = 0
				} // if

				if tmpQL > tmpQC {
					tmpQL = tmpQC
				} // if

				if tmpQH < tmpQC {
					tmpQH = tmpQC
				} // if

				outArr = append(outArr, Quote{Symbol: symbol,
					QuoteDate: tmpQD,
					Open:      tmpQO,
					High:      tmpQH,
					Low:       tmpQL,
					Close:     tmpQC,
					Volume:    tmpQV,
				})
			} // if
		} // if
	} // for

	return
} // UpdateYahoo ()

// UpdateStooq reads values from stooq.com and returns a slice of quotes
// symbol should be in the format "stooq::<symbol>"
func UpdateStooq(symbol string) (outArr []Quote) {
	var startDate time.Time
	var tmpQV int64
	// get last quote for symbol and extract quote date
	rows := FindQuotes(symbol, "ASC")

	// get last quote date, if we already have quotes
	if len(rows) > 0 {
		startDate = rows[len(rows)-1].QuoteDate
	} // if

	fmt.Println("Last quote date before update: " + startDate.Format(time.RFC3339))
	// create URL to grep and download data
	// s = symbol
	// i = interval (<d>aily, <w>eekly, <m>onthly, <q>uaterly, <y>early)
	// c = comma(not set), semicolon (1)
	// https://stooq.com/q/d/l/?s=hrpk.de&i=d&c=1
	// https://stooq.com/q/d/l/?s=hrpk.de&d1=20090102&d2=20240122&i=d&c=1
	stream, err := http.Get(fmt.Sprintf("https://stooq.com/q/d/l/?s=%s&i=d&c=1", strings.Split(symbol, "::")[1]))
	misc.ShowError(err, "", "ErrPanic")
	content, err := io.ReadAll(stream.Body)
	misc.ShowError(err, "", "ErrPanic")
	err = stream.Body.Close()
	misc.ShowError(err, "", "ErrPanic")
	contentString := string(content)
	//fmt.Println(contentString)

	// split content lines
	for i, v := range strings.Split(contentString, "\r\n") {
		// skip header lines and line is not empty
		if i > 2 && len(v) > 0 {
			lineSplit := strings.Split(v, ";")
			tmpQD, err := time.Parse("2006-01-02", lineSplit[0])
			misc.ShowError(err, "", "ErrPrint")

			if err == nil {
				tmpQO, err := strconv.ParseFloat(lineSplit[1], 64)
				misc.ShowError(err, "", "ErrPrint")
				tmpQH, err := strconv.ParseFloat(lineSplit[2], 64)
				misc.ShowError(err, "", "ErrPrint")
				tmpQL, err := strconv.ParseFloat(lineSplit[3], 64)
				misc.ShowError(err, "", "ErrPrint")
				tmpQC, err := strconv.ParseFloat(lineSplit[4], 64)
				misc.ShowError(err, "", "ErrPrint")

				if len(lineSplit) > 5 {
					tmpQV, err = strconv.ParseInt(lineSplit[5], 10, 64)
					misc.ShowError(err, "", "ErrPrint")
				} else {
					tmpQV = 0
				} // if

				outArr = append(outArr, Quote{Symbol: symbol,
					QuoteDate: tmpQD,
					Open:      tmpQO,
					High:      tmpQH,
					Low:       tmpQL,
					Close:     tmpQC,
					Volume:    tmpQV,
				})
			} // if
		} // if
	} // for

	return
} // UpdateStooq ()

// UpdateSwisscanto reads values from cash.ch or www.swissfunddata.ch and returns a slice of quotes
// symbol should be in the format "swisscanto::<fundid>"
func UpdateSwisscanto(symbol string) (outArr []Quote) {
	var startDate time.Time
	// get last quote for symbol and extract quote date
	rows := FindQuotes(symbol, "ASC")

	// get last quote date, if we already have quotes
	if len(rows) > 0 {
		startDate = rows[len(rows)-1].QuoteDate
	} // if

	fmt.Println("Last quote date before update: " + startDate.Format(time.RFC3339))
	// create URL to grep and download data
	stream, err := http.Get(fmt.Sprintf("https://www.swissfunddata.ch/sfdpub/en/funds/excelData/%s", strings.Split(strings.Split(symbol, ".")[0], "::")[1]))
	misc.ShowError(err, "", "ErrPanic")
	content, err := io.ReadAll(stream.Body)
	misc.ShowError(err, "", "ErrPanic")
	err = stream.Body.Close()
	misc.ShowError(err, "", "ErrPanic")
	contentString := string(content)

	// split content lines
	for i, v := range strings.Split(contentString, "\n") {
		// skip header lines and line is not empty
		if i > 2 && len(v) > 0 {
			lineSplit := strings.Split(v, ";")
			tmpQD, err := time.Parse("2006-01-02", lineSplit[0])
			misc.ShowError(err, "", "ErrPrint")

			if err == nil {
				tmpQC, err := strconv.ParseFloat(lineSplit[2], 64)
				misc.ShowError(err, "", "ErrPrint")
				outArr = append(outArr, Quote{Symbol: symbol,
					QuoteDate: tmpQD,
					Open:      tmpQC,
					High:      tmpQC,
					Low:       tmpQC,
					Close:     tmpQC,
					Volume:    0.0,
				})
			} // if
		} // if
	} // for

	return
} // UpdateSwisscanto ()

// UpdateAriva reads values from ariva.de and returns a slice of quotes
// symbol should be in the format "ariva::<secu>.<boerse_id>"
func UpdateAriva(symbol string) (outLookup []Quote) {
	var (
		startDate time.Time
		boerse    string
	)
	// get last quote for symbol and extract quote date
	rows := FindQuotes(symbol, "ASC")

	// get last quote date, if we already have quotes
	if len(rows) > 0 {
		startDate = rows[len(rows)-1].QuoteDate
	} // if

	fmt.Println("Last quote date before update: " + startDate.Format(time.RFC3339))
	// sub (day - 7 days) a week to date to make sure we get the quotes
	startDate = startDate.AddDate(0, 0, -7)
	// get current date
	endDate := time.Now()
	// add (day + 1 day) one day so ariva gives us the current day as well
	endDate = endDate.AddDate(0, 0, 1)
	// extract secu and boerse_id from symbol
	tmpSplit := strings.Split(symbol, ".")

	if len(tmpSplit) > 1 {
		boerse = tmpSplit[1]
	} else {
		misc.ShowError(nil, "Splitting of quotes for "+symbol+" did not succeed", "ErrFatal")
	} // if

	secu := strings.Split(strings.Split(symbol, ".")[0], "::")[1]

	// create URL to grep and download data
	//	fmt.Printf("https://www.ariva.de/quote/historic/historic.csv?secu=%s&boerse_id=%s&min_time=%d.%d.%d&max_time=%d.%d.%d&clean_split=1&clean_payout=0&clean_bezug=0&go=Download", secu, boerse, startDate.Day(), startDate.Month(), startDate.Year(), endDate.Day(), endDate.Month(), endDate.Year())
	stream, err := http.Get(fmt.Sprintf("https://www.ariva.de/quote/historic/historic.csv?secu=%s&boerse_id=%s&min_time=%d.%d.%d&max_time=%d.%d.%d&clean_split=1&clean_payout=0&clean_bezug=0&go=Download", secu, boerse, startDate.Day(), startDate.Month(), startDate.Year(), endDate.Day(), endDate.Month(), endDate.Year()))
	misc.ShowError(err, "", "ErrPanic")
	content, err := io.ReadAll(stream.Body)
	misc.ShowError(err, "", "ErrPanic")
	err = stream.Body.Close()
	misc.ShowError(err, "", "ErrPanic")
	contentString := string(content)

	if contentString == "" {
		fmt.Println("Content for symbol " + symbol + " is empty. Please check symbol.")
	} else {
		// extract data from HTML
		lookup := GetSingleLookup(symbol)
		var searchWord = "Volumen"

		// if security is a currency it has no volume => look for Schlusskurs
		if lookup.LookType == "Currency" {
			searchWord = "Schlusskurs"
		} // if

		// Remove header line
		idx := strings.Index(contentString, searchWord)
		contentString = contentString[idx+len([]byte(searchWord)):]
		// Replace blanks with 0s
		contentString = strings.ReplaceAll(contentString, "; ", ";0")

		// split content at each day
		for _, v := range strings.Fields(contentString) {
			// remove thousand seperators (if present)
			v = strings.ReplaceAll(v, ".", "")
			// replace all commas with decimal points
			v = strings.ReplaceAll(v, ",", ".")

			// check if the line contains ";" and thus is not empty
			if strings.Contains(v, ";") {
				// split values and put them into a Quote
				lineSplit := strings.Split(v, ";")
				tmpQD, err := time.Parse("2006-01-02", lineSplit[0])
				misc.ShowError(err, "", "ErrPanic")
				tmpQO := stringToFloat(lineSplit[1])
				tmpQH := stringToFloat(lineSplit[2])
				tmpQL := stringToFloat(lineSplit[3])
				tmpQC := stringToFloat(lineSplit[4])

				tmpQO = secureValues(tmpQO, tmpQC)
				tmpQL = secureValues(tmpQL, tmpQC)
				tmpQH = secureValues(tmpQH, tmpQC)
				tmpQL = lowest(tmpQO, tmpQH, tmpQL, tmpQC)
				tmpQH = highest(tmpQO, tmpQH, tmpQL, tmpQC)
				var tmpQV int64 = 0

				// Check if found value for volume is valid
				if lookup.LookType != "Currency" && lineSplit[5] != "" {
					tmpQV, err = strconv.ParseInt(lineSplit[5], 10, 64)
					misc.ShowError(err, "", "ErrPanic")

					if tmpQV < 0 {
						tmpQV = 0
					} // if
				} // if

				outLookup = append(outLookup, Quote{Symbol: symbol,
					QuoteDate: tmpQD,
					Open:      tmpQO,
					High:      tmpQH,
					Low:       tmpQL,
					Close:     tmpQC,
					Volume:    tmpQV,
				})
			} // if
		} // for
	} // if

	return
} // UpdateAriva ()

// stringToFloat Simplifies the error handling for the string to float conversion
// if called multiple times in a row
func stringToFloat(input string) (ret float64) {
	ret, err := strconv.ParseFloat(input, 64)
	misc.ShowError(err, "", "ErrPanic")
	return
} // stringToFloat ()

// secureValues checks given input (open, high and low) if it is equal to 0.0
// and if this is the case returns the closing value
func secureValues(tmpInp, tmpQC float64) (tmpVal float64) {
	tmpVal = tmpInp

	if tmpInp == 0.0 {
		tmpVal = tmpQC
	} // if

	return
} // secureValues ()

// Return the lowest of the given values
func lowest(inp ...float64) (low float64) {
	low = -1.0

	for _, v := range inp {
		if low == -1.0 || v < low {
			low = v
		} // if
	} // for

	return
} // lowest ()

// Return the highest of the given values
func highest(inp ...float64) (high float64) {
	high = -1.0

	for _, v := range inp {
		if high == -1.0 || v > high {
			high = v
		} // if
	} // for

	return
} // highest ()

func UpdateQuoteLoop(allLookup []Lookup) {
	var (
		tmpValues []Quote
		loopCount = 0
	)

	lenLookup := len(allLookup)

	for i := range allLookup {
		// Skip ariva symbols for now
		if len(allLookup[i].Symbol) > 6 && allLookup[i].Symbol[:5] != "ariva" {
			fmt.Printf("Element %d of %d: %s\n", i+1, lenLookup, allLookup[i].Name+"("+allLookup[i].Symbol+"/"+allLookup[i].ISIN+")")

			switch {
			// Currently disabled
			//		case len(allLookup[i].Symbol) > 6 && allLookup[i].Symbol[:5] == "ariva":
			//			tmpValues = UpdateAriva(allLookup[i].Symbol)
			case len(allLookup[i].Symbol) > 6 && allLookup[i].Symbol[:5] == "stooq":
				tmpValues = UpdateStooq(allLookup[i].Symbol)
			case len(allLookup[i].Symbol) > 6 && allLookup[i].Symbol[:5] == "yahoo":
				tmpValues = UpdateYahoo(allLookup[i].Symbol)
			case len(allLookup[i].Symbol) > 12 && allLookup[i].Symbol[:10] == "swisscanto":
				tmpValues = UpdateSwisscanto(allLookup[i].Symbol)
			default:
				fmt.Println("No update function for symbol: " + allLookup[i].Symbol)
			} // switch

			// Update quotes if we have new values
			if len(tmpValues) != 0 {
				UpsertQuotes(allLookup[i].Symbol, tmpValues)
				tmpDate := GetLastQuoteDate(allLookup[i].Symbol)
				fmt.Println("Last quote date after update: " + tmpDate)
			} else {
				fmt.Println("No new values")
			} // if

			loopCount++
			loopCount = sleepCycle(loopCount)
		} // if
	} // for
} // UpdateQuoteLoop ()

// Sleep for random time to avoid being blocked by server
func sleepCycle(loopCount int) int {
	var (
		duration *big.Int
		err      error
	)

	const (
		maxElemCount = 20
		minRandSym   = 5
		maxRandSym   = 12
		minRand      = 5
		maxRand      = 30
	)

	switch {
	case loopCount < maxElemCount:
		duration, err = rand.Int(rand.Reader, big.NewInt(maxRandSym))
		misc.ShowError(err, "", "ErrPanic")
		duration = duration.Add(duration, big.NewInt(minRandSym))
		loopCount++
	default:
		duration, err = rand.Int(rand.Reader, big.NewInt(maxRand))
		misc.ShowError(err, "", "ErrPanic")
		duration = duration.Add(duration, big.NewInt(minRand))
		loopCount = 0
	} // switch

	fmt.Printf("Sleeping for %d seconds\n\n", duration)
	time.Sleep(time.Duration(duration.Int64()) * time.Second)
	return loopCount
} // sleepCycle ()
