// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/bluebottle/go-modules/misc"
)

// Testing WeeklyQuotes function.
func TestWeeklyQuotes(t *testing.T) {
	tmpTime, err := time.Parse("2006-01-02", "2020-01-03")
	misc.ShowError(err, "", "ErrPanic")
	quoteVal := []Quote{
		{Symbol: "Test", QuoteDate: tmpTime, Open: 127.0000, High: 127.7154, Low: 126.8597, Close: 127.2876, Volume: 0},
		{Symbol: "Test", QuoteDate: tmpTime.AddDate(0, 0, 3), Open: 127.1020, High: 127.6855, Low: 126.6309, Close: 127.1781, Volume: 0},
		{Symbol: "Test", QuoteDate: tmpTime.AddDate(0, 0, 4), Open: 127.1020, High: 128.2228, Low: 126.8001, Close: 128.0138, Volume: 0},
		{Symbol: "Test", QuoteDate: tmpTime.AddDate(0, 0, 5), Open: 127.1020, High: 128.2725, Low: 126.7105, Close: 127.1085, Volume: 0},
		{Symbol: "Test", QuoteDate: tmpTime.AddDate(0, 0, 6), Open: 127.1020, High: 128.0934, Low: 126.8001, Close: 127.7253, Volume: 0},
		{Symbol: "Test", QuoteDate: tmpTime.AddDate(0, 0, 7), Open: 127.1020, High: 128.2725, Low: 126.1335, Close: 127.0587, Volume: 0},
		{Symbol: "Test", QuoteDate: tmpTime.AddDate(0, 0, 10), Open: 128.1020, High: 128.6406, Low: 127.3970, Close: 128.2725, Volume: 0},
	}
	result := []Quote{
		{"Test", tmpTime, 127, 127.7154, 126.8597, 127.2876, 0},
		{"Test", tmpTime.AddDate(0, 0, 3), 127.102, 128.2725, 126.1335, 127.0587, 0},
		{"Test", tmpTime.AddDate(0, 0, 10), 128.102, 128.6406, 127.397, 128.2725, 0},
	}

	weekValues := WeeklyQuotes(quoteVal)

	if !reflect.DeepEqual(weekValues, result) {
		t.Errorf("WeeklyQuotes was incorrect, got: %v, want: %v.", weekValues, result)
	} // if
} // TestWeeklyQuotes ()

// Currently not needed
// func TestToStringSlice(t *testing.T) {
// 	tmpTime, err := time.Parse("2006-01-02", "2020-01-03")
// 	misc.ErrorPanic(err)
// 	quoteVal := Quote{Symbol: "Test", QuoteDate: tmpTime, Open: 127.0000, High: 127.7154, Low: 126.8597, Close: 127.2876, Volume: 0}
// 	total := quoteVal.ToStringSlice()
// 	result := []string{"2020-01-03 00:00:00 +0000 UTC", "127.000000", "127.287600", "127.715400", "126.859700", "0"}

// 	if !reflect.DeepEqual(total, result) {
// 		t.Errorf("ToStringSlice was incorrect, got: %v, want: %v.", total, result)
// 	} // if
// } // TestToStringSlice ()

func TestGetClose(t *testing.T) {
	type testValues struct {
		values []Quote
		close  []float64
	}

	testVal := []testValues{
		{[]Quote{{Close: 10.0},
			{Close: 20.0},
			{Close: 30.0},
			{Close: 40.0},
			{Close: 50.0},
		},
			[]float64{10.0, 20.0, 30.0, 40.0, 50.0}},
		{[]Quote{{Close: 20.0},
			{Close: 40.0},
			{Close: 60.0},
			{Close: 80.0},
			{Close: 100.0},
		},
			[]float64{20.0, 40.0, 60.0, 80.0, 100.0}},
	}

	for _, test := range testVal {
		results := GetClose(test.values)

		if !reflect.DeepEqual(results, test.close) {
			t.Errorf("getClose was incorrect, got: %v, want: %v.", results, test.close)
		} // if
	} // for
} // TestGetClose ()
