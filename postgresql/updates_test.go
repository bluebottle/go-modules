// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"reflect"
	"testing"
)

func TestLowest(t *testing.T) {
	tables := []struct {
		values []float64
		result float64
	}{
		{[]float64{11, 12, 13, 14, 15, 16, 17}, 11.0},
		{[]float64{}, -1.0},
	}

	for _, table := range tables {
		total := lowest(table.values...)

		if !reflect.DeepEqual(total, table.result) {
			t.Errorf("lowest was incorrect, got: %v, want: %v.", total, table.result)
		} // if
	} // for
} // Testlowest ()

func TestHighest(t *testing.T) {
	tables := []struct {
		values []float64
		result float64
	}{
		{[]float64{11, 12, 13, 14, 15, 16, 17}, 17.0},
		{[]float64{}, -1.0},
	}

	for _, table := range tables {
		total := highest(table.values...)

		if !reflect.DeepEqual(total, table.result) {
			t.Errorf("highest was incorrect, got: %v, want: %v.", total, table.result)
		} // if
	} // for
} // Testhighest ()
