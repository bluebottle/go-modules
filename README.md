These are some packages I was using in different projects, requiring to sync updates to the packages between projects. Thus implmenting them as modules made sense and certainly easier to maintain.

The modules are:

| Module | Description |
| :--- | :--- |
| **misc** | Short snippets not fitting anywhere else. |
| **postgresql** | Functions using PostgreSQL, specially tailored for my trading projects |
| **technical** | Some functions used for my trading projects, like indicators and portfolio functions |
